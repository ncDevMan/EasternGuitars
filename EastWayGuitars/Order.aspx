﻿<%@ Page Title="Order" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Order.aspx.cs" Inherits="EastWayGuitars.Order" %>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1>Order A Guitar</h1>
    </hgroup>

      <aside>
        <h3>Guitar Parts</h3>
        <ul>
            <asp:Placeholder ID ="PlaceHolder2" runat="server" />
        </ul>
          <br />
          <br />
          <br />
          <br />
          <br />

        <p>Your Suppliers for the Parts and Guitars are as follows:</p>
         <ul>
            <asp:Placeholder ID ="PlaceHolder3" runat="server" />
        </ul>


    </aside>
    <h3>Featured Guitars</h3>
    <article>
        <p>
            <asp:Placeholder ID ="PlaceHolder1" runat="server" />
        </p>
    </article>


    <br />
    <br />

    <%--<article> </article>--%>
        <table class="auto-style1">
            <tr>
                <td class="auto-style5">
                    <table class="auto-style1">
                        <tr>
                            <td>First Name*</td>
                        </tr>
                    </table>
                </td>
                <td class="auto-style6">
                    <asp:TextBox ID="firstNameBox" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style5">
                    <table class="auto-style1">
                        <tr>
                            <td>Last Name*</td>
                        </tr>
                    </table>
                </td>
                <td class="auto-style6">
                    <asp:TextBox ID="lastNameBox" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Address*</td>
                <td class="auto-style4">
                    <asp:TextBox ID="addressBox" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Email*</td>
                <td>
                    <asp:TextBox ID="emailBox" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Phone Number*</td>
                <td>
                    <asp:TextBox ID="numberBox" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Please Choose a Guitar*</td>
                <td>
                    <asp:DropDownList id="guitarList" runat="server">
                     <asp:ListItem>Martin D-15</asp:ListItem>
                     <asp:ListItem>Martin D-18</asp:ListItem>
                     <asp:ListItem>Taylor 914CE</asp:ListItem>
                   </asp:DropDownList>
                </td>
            </tr>

            <tr>
                <td class="auto-style2">Part (Optional)</td>
                <td>
                    <asp:DropDownList id="partList1" runat="server">
                     <asp:ListItem>None</asp:ListItem>
                     <asp:ListItem>Martin Acoustic Medium Guitar Strings</asp:ListItem>
                     <asp:ListItem>Martin Acoustic Heavy Guitar Strings</asp:ListItem>
                     <asp:ListItem>Martin Black Bridge Pins</asp:ListItem>
                     <asp:ListItem>Martin White Bridge Pins</asp:ListItem>
                   </asp:DropDownList>
                </td>
            </tr>
            <br />
            <br />
            <tr>
                <td class="auto-style2">Part (Optional)</td>
                <td>
                    <asp:DropDownList id="partList2" runat="server">
                     <asp:ListItem>None</asp:ListItem>
                     <asp:ListItem>Martin Acoustic Medium Guitar Strings</asp:ListItem>
                     <asp:ListItem>Martin Acoustic Heavy Guitar Strings</asp:ListItem>
                     <asp:ListItem>Martin Black Bridge Pins</asp:ListItem>
                     <asp:ListItem>Martin White Bridge Pins</asp:ListItem>
                   </asp:DropDownList>
                </td>
            </tr>



            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Insert" />
                </td>
            </tr>


        </table>

 
</asp:Content>