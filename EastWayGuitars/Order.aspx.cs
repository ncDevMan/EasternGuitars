﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Text;
using MySql.Data.MySqlClient;

namespace EastWayGuitars
{
    public partial class Order : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //This will grab the data from the guitar table
            loadGuitars();

            //This will grab the data from the parts table
            loadParts();

            loadSuppliers();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //All the text boxes
            string firstName, lastName, address, email, phoneNumber, guitarSelected, partList1Text, partList2Text, monthOfOrder;
            firstName = firstNameBox.Text;
            lastName = lastNameBox.Text;
            address = addressBox.Text;
            email = emailBox.Text;
            phoneNumber = numberBox.Text;

            //DropDown Boxes
            guitarSelected = guitarList.SelectedValue;
            partList1Text = partList1.SelectedValue;
            partList2Text = partList2.SelectedValue;

            //Stuff we need to add to the database for our sale analysis
            double totalPrice = 0.00;
            int totalParts = 0;

            monthOfOrder = DateTime.Now.ToString("MMMM");

            //Let's make some switch statements for our dropdown boxes.
            switch (guitarSelected)
            {
                case "Martin D-15":
                    totalPrice = totalPrice + 1500.00;
                    break;

                case "Martin D-18":
                    totalPrice = totalPrice + 1800.00;
                    break;

                case "Taylor 914CE":
                    totalPrice = totalPrice + 4300.00;
                    break;
            }


            //Dropdown box Part - 1
            switch (partList1Text){
                case "None":
                    totalPrice = totalPrice + 0.00;
                    break;

                case "Martin Acoustic Light Guitar Strings":
                    totalPrice = totalPrice + 7.00;
                    totalParts++;
                    break;

                case "Martin Acoustic Medium Guitar Strings":
                    totalPrice = totalPrice + 7.00;
                    totalParts++;
                    break;

                case "Martin Acoustic Heavy Guitar Strings":
                    totalPrice = totalPrice + 9.00;
                    totalParts++;
                    break;

                case "Martin Black Bridge Pins":
                    totalPrice = totalPrice + 13.00;
                    totalParts++;
                    break;

                case "Martin White Bridge Pins":
                    totalPrice = totalPrice + 13.00;
                    totalParts++;
                    break;  
            }

            //Dropdown box Part - 2
            switch (partList2Text)
            {
                case "None":
                    totalPrice = totalPrice + 0.00;
                    break;

                case "Martin Acoustic Light Guitar Strings":
                    totalPrice = totalPrice + 7.00;
                    totalParts++;
                    break;

                case "Martin Acoustic Medium Guitar Strings":
                    totalPrice = totalPrice + 7.00;
                    totalParts++;
                    break;

                case "Martin Acoustic Heavy Guitar Strings":
                    totalPrice = totalPrice + 9.00;
                    totalParts++;
                    break;

                case "Martin Black Bridge Pins":
                    totalPrice = totalPrice + 13.00;
                    totalParts++;
                    break;

                case "Martin White Bridge Pins":
                    totalPrice = totalPrice + 13.00;
                    totalParts++;
                    break;
            }

            //Create a random shipping number
            Random shippingNumber = new Random();
            int shippingNumberFinal = shippingNumber.Next(99999);

            //Database connection here
            String str = @"server=localhost;database=alohainventory;userid=root;password=;";
            //String str = @"server=csshrpt.eku.edu;user=csc440;database=csc440;port=3306;password=CSC440student;"

            MySqlConnection con = null;

            try
            {
                con = new MySqlConnection(str);
                con.Open(); //Open connection

                String stmt = "INSERT INTO conn_customers(firstName, lastName, address, email, phoneNumber) VALUES(@firstName, @lastName, @address, @email, @phoneNumber)";
                MySqlCommand cmd = new MySqlCommand(stmt, con);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@firstName", firstName);
                cmd.Parameters.AddWithValue("@lastName", lastName);
                cmd.Parameters.AddWithValue("@address", address);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@phoneNumber", phoneNumber);
                cmd.ExecuteNonQuery();

            }
            catch (MySqlException err)
            { //We will capture and display any MySql errors that will occur

                Console.WriteLine("Error: " + err.ToString());
            }
            finally
            {
                if (con != null)
                {
                    con.Close(); //safely close the connection
                }
            } //remember to safely close the connection after accessing the database

            try
            {
                con = new MySqlConnection(str);
                con.Open(); //Open connection

                String stmt2 = "INSERT INTO conn_orders(shippingNumber, totalPrice, monthOfOrder, totalParts) VALUES(@shippingNumber, @totalPrice, @monthOfOrder, @totalParts)";
                MySqlCommand cmd2 = new MySqlCommand(stmt2, con);
                cmd2.Prepare();
                cmd2.Parameters.AddWithValue("@shippingNumber", shippingNumberFinal);
                cmd2.Parameters.AddWithValue("@totalPrice", totalPrice);
                cmd2.Parameters.AddWithValue("@monthOfOrder", monthOfOrder);
                cmd2.Parameters.AddWithValue("@totalParts", totalParts);
                cmd2.ExecuteNonQuery();

            }
            catch (MySqlException err)
            { //We will capture and display any MySql errors that will occur

                Console.WriteLine("Error: " + err.ToString());
            }
            finally
            {
                if (con != null)
                {
                    con.Close(); //safely close the connection
                }
            } //remember to safely close the connection after accessing the database


            //Redirect after the submit button is pressed.
            Response.Redirect("~/Results.aspx");


        }

        private DataTable getSupplierData() {
            //Database connection here
            String connectionString = @"server=localhost;database=alohainventory;userid=root;password=;";
            //String connectionString = @"server=csshrpt.eku.edu;user=csc440;database=csc440;port=3306;password=CSC440student;"

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                //Enter your SQL Command here
                using (MySqlCommand cmd = new MySqlCommand("SELECT name, address, phoneNumber FROM conn_suppliers"))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        cmd.Connection = conn;
                        sda.SelectCommand = cmd;
                        //This will make a neat datatable outputting on the web form. 
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        
        }

        private void loadSuppliers() {
            //Security Setting.
            //Postback checks if the page is on initial load or someone has pressed a button.
            //It is a boolean
            if (!this.IsPostBack)
            {
                //Populating a DataTable from database.
                DataTable dt = this.getSupplierData();

                //Building an HTML string.
                StringBuilder html = new StringBuilder();

                //HTML Table start tag
                //Lets make it border = 1 shall we?
                html.Append("<table border = '1'>");

                //Building the Header row.
                html.Append("<tr>");
                foreach (DataColumn column in dt.Columns)
                {
                    html.Append("<th>");
                    html.Append(column.ColumnName);
                    html.Append("</th>");
                }
                //HTML table row header end
                html.Append("</tr>");

                //Building the Data rows.
                foreach (DataRow row in dt.Rows)
                {
                    //HTML Table row header start
                    html.Append("<tr>");
                    foreach (DataColumn column in dt.Columns)
                    {
                        //HTML table data
                        html.Append("<td>");
                        html.Append(row[column.ColumnName]);
                        html.Append("<br />");
                        html.Append("</td>");
                    }
                    //HTML table row header end
                    html.Append("</tr>");
                }

                //HTML Table End tag
                html.Append("</table>");

                //Append the HTML string to Placeholder.
                PlaceHolder3.Controls.Add(new Literal { Text = html.ToString() });
            }
        }


        /*
         * loadParts() Function
         *
         * @return: Void
         * 
         * @author: Nick
         * 
         * This function will call the parts table from the database.
         * It is then placed into a c# library to create a data table- allowing better viewing for the user. 
         */
        private void loadParts()
        {
            //Security Setting.
            //Postback checks if the page is on initial load or someone has pressed a button.
            //It is a boolean
            if (!this.IsPostBack)
            {
                //Populating a DataTable from database.
                DataTable dt = this.getPartData();

                //Building an HTML string.
                StringBuilder html = new StringBuilder();

                //HTML Table start tag
                //Lets make it border = 1 shall we?
                html.Append("<table border = '1'>");

                //Building the Header row.
                html.Append("<tr>");
                foreach (DataColumn column in dt.Columns)
                {
                    html.Append("<th>");
                    html.Append(column.ColumnName);
                    html.Append("</th>");
                }
                //HTML table row header end
                html.Append("</tr>");

                //Building the Data rows.
                foreach (DataRow row in dt.Rows)
                {
                    //HTML Table row header start
                    html.Append("<tr>");
                    foreach (DataColumn column in dt.Columns)
                    {
                        //HTML table data
                        html.Append("<td>");
                        html.Append(row[column.ColumnName]);
                        html.Append("<br />");
                        html.Append("</td>");
                    }
                    //HTML table row header end
                    html.Append("</tr>");
                }

                //HTML Table End tag
                html.Append("</table>");

                //Append the HTML string to Placeholder.
                PlaceHolder2.Controls.Add(new Literal { Text = html.ToString() });
            }
        }


        /*
         * loadGuitars() Function
         *
         * @return: Void
         * 
         * @author: Nick
         * 
         * This function will call the guitars table from the database.
         * It is then placed into a c# library to create a data table- allowing better viewing for the user. 
         */
        private void loadGuitars()
        {
            //Security Setting.
            //Postback checks if the page is on initial load or someone has pressed a button.
            //It is a boolean
            if (!this.IsPostBack)
            {
                //Populating a DataTable from database.
                DataTable dt = this.getGuitarData();

                //Building an HTML string.
                StringBuilder html = new StringBuilder();

                //HTML Table start tag
                //Lets make it border = 1 shall we?
                html.Append("<table border = '1'>");

                //Building the Header row.
                html.Append("<tr>");
                foreach (DataColumn column in dt.Columns)
                {
                    html.Append("<th>");
                    html.Append(column.ColumnName);
                    html.Append("</th>");
                }
                //HTML table row header end
                html.Append("</tr>");

                //Building the Data rows.
                foreach (DataRow row in dt.Rows)
                {
                    //HTML Table row header start
                    html.Append("<tr>");
                    foreach (DataColumn column in dt.Columns)
                    {
                        //HTML table data
                        html.Append("<td>");
                        html.Append(row[column.ColumnName]);
                        html.Append("<br />");
                        html.Append("</td>");
                    }
                    //HTML table row header end
                    html.Append("</tr>");
                }

                //HTML Table End tag
                html.Append("</table>");

                //Append the HTML string to Placeholder.
                PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
            }
        }


        private DataTable getGuitarData()
        {
            //Database connection here
            String connectionString = @"server=localhost;database=alohainventory;userid=root;password=;";
            //String connectionString = @"server=csshrpt.eku.edu;user=csc440;database=csc440;port=3306;password=CSC440student;"

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                //Enter your SQL Command here
                using (MySqlCommand cmd = new MySqlCommand("SELECT name, model, price FROM conn_guitars"))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        cmd.Connection = conn;
                        sda.SelectCommand = cmd;
                        //This will make a neat datatable outputting on the web form. 
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }

        private DataTable getPartData()
        {
            //Database connection here
            String connectionString = @"server=localhost;database=alohainventory;userid=root;password=;";
            //String connectionString = @"server=csshrpt.eku.edu;user=csc440;database=csc440;port=3306;password=CSC440student;"

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                //Enter your SQL Command here
                using (MySqlCommand cmd = new MySqlCommand("SELECT name, price FROM conn_parts"))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        cmd.Connection = conn;
                        sda.SelectCommand = cmd;
                        //This will make a neat datatable outputting on the web form. 
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }
    }
}