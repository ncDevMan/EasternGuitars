﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Text;
using MySql.Data.MySqlClient;


namespace EastWayGuitars
{
    public partial class Results : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadCommission();

            loadMonthlySales();

            loadTotalParts();

        }

        private DataTable getTotalPartsData()
        {

            //Database connection here
            String connectionString = @"server=localhost;database=alohainventory;userid=root;password=;";
            //String connectionString = @"server=csshrpt.eku.edu;user=csc440;database=csc440;port=3306;password=CSC440student;"

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                //Enter your SQL Command here
                using (MySqlCommand cmd = new MySqlCommand("SELECT SUM(totalParts) AS finalTotalParts FROM conn_orders WHERE `monthOfOrder` = @currentMonth"))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        string currentMonth = DateTime.Now.ToString("MMMM");
                        cmd.Parameters.AddWithValue("@currentMonth", currentMonth);

                        cmd.Connection = conn;
                        sda.SelectCommand = cmd;
                        //This will make a neat datatable outputting on the web form. 
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }

        }

        private void loadTotalParts() {
            //Security Setting.
            //Postback checks if the page is on initial load or someone has pressed a button.
            //It is a boolean
            if (!this.IsPostBack)
            {
                //Populating a DataTable from database.
                DataTable dt = this.getTotalPartsData();

                //Building an HTML string.
                StringBuilder html = new StringBuilder();

                //HTML Table start tag
                //Lets make it border = 1 shall we?
                html.Append("<table border = '1'>");

                //Building the Header row.
                //html.Append("<tr>");
                //foreach (DataColumn column in dt.Columns)
                //{
                //    html.Append("<th>");
                //    html.Append(column.ColumnName);
                //    html.Append("</th>");
                //}
                ////HTML table row header end
                //html.Append("</tr>");

                //Building the Data rows.
                foreach (DataRow row in dt.Rows)
                {
                    //HTML Table row header start
                    html.Append("<tr>");
                    foreach (DataColumn column in dt.Columns)
                    {
                        //HTML table data
                        html.Append("<td>");
                        html.Append(row[column.ColumnName]);
                        html.Append("<br />");
                        html.Append("</td>");
                    }
                    //HTML table row header end
                    html.Append("</tr>");
                }

                //HTML Table End tag
                html.Append("</table>");

                //Append the HTML string to Placeholder.
                PlaceHolder3.Controls.Add(new Literal { Text = html.ToString() });
            }

        }

        private DataTable getCommissionData(){

            //Database connection here
            String connectionString = @"server=localhost;database=alohainventory;userid=root;password=;";
            //String connectionString = @"server=csshrpt.eku.edu;user=csc440;database=csc440;port=3306;password=CSC440student;"

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                //Enter your SQL Command here
                using (MySqlCommand cmd = new MySqlCommand("SELECT SUM(totalPrice)*0.02 AS commissionPrice FROM conn_orders WHERE `monthOfOrder` = @currentMonth"))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        string currentMonth = DateTime.Now.ToString("MMMM");
                        cmd.Parameters.AddWithValue("@currentMonth", currentMonth);

                        cmd.Connection = conn;
                        sda.SelectCommand = cmd;
                        //This will make a neat datatable outputting on the web form. 
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }

        }

        private void loadCommission()
        {
            //Security Setting.
            //Postback checks if the page is on initial load or someone has pressed a button.
            //It is a boolean
            if (!this.IsPostBack)
            {
                //Populating a DataTable from database.
                DataTable dt = this.getCommissionData();

                //Building an HTML string.
                StringBuilder html = new StringBuilder();

                //HTML Table start tag
                //Lets make it border = 1 shall we?
                html.Append("<table border = '1'>");

                //Building the Header row.
                //html.Append("<tr>");
                //foreach (DataColumn column in dt.Columns)
                //{
                //    html.Append("<th>");
                //    html.Append(column.ColumnName);
                //   html.Append("</th>");
                //}
                ////HTML table row header end
                //html.Append("</tr>");

                //Building the Data rows.
                foreach (DataRow row in dt.Rows)
                {
                    //HTML Table row header start
                    html.Append("<tr>");
                    foreach (DataColumn column in dt.Columns)
                    {
                        //HTML table data
                        html.Append("<td>");
                        html.Append("$"+row[column.ColumnName]);
                        html.Append("<br />");
                        html.Append("</td>");
                    }
                    //HTML table row header end
                    html.Append("</tr>");
                }

                //HTML Table End tag
                html.Append("</table>");

                //Append the HTML string to Placeholder.
                PlaceHolder2.Controls.Add(new Literal { Text = html.ToString() });
            }
        }

        private DataTable getMonthlyData()
        {
            //Database connection here
            String connectionString = @"server=localhost;database=alohainventory;userid=root;password=;";
            //String connectionString = @"server=csshrpt.eku.edu;user=csc440;database=csc440;port=3306;password=CSC440student;"

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                //Enter your SQL Command here
                using (MySqlCommand cmd = new MySqlCommand("SELECT shippingNumber, totalPrice, monthOfOrder FROM conn_orders WHERE monthOfOrder = @currentMonth"))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {

                        //This will Grab the current month as a string
                        string currentMonth = DateTime.Now.ToString("MMMM");
                        cmd.Parameters.AddWithValue("@currentMonth", currentMonth);

                        cmd.Connection = conn;
                        sda.SelectCommand = cmd;
                        //This will make a neat datatable outputting on the web form. 
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }

        private void loadMonthlySales()
        {
            //Security Setting.
            //Postback checks if the page is on initial load or someone has pressed a button.
            //It is a boolean
            if (!this.IsPostBack)
            {
                //Populating a DataTable from database.
                DataTable dt = this.getMonthlyData();

                //Building an HTML string.
                StringBuilder html = new StringBuilder();

                //HTML Table start tag
                //Lets make it border = 1 shall we?
                html.Append("<table border = '3'>");

                //Building the Header row.
                html.Append("<tr>");
                foreach (DataColumn column in dt.Columns)
                {
                    html.Append("<th>");
                    html.Append(column.ColumnName + " " + " " + " " + " " + " ");
                    html.Append("<br />");
                    html.Append("</th>");
                }


                //HTML table row header end
                html.Append("</tr>");
                
                //Building the Data rows.
                foreach (DataRow row in dt.Rows)
                {
                    //HTML Table row header start
                    html.Append("<tr>");
                    foreach (DataColumn column in dt.Columns)
                    {
                        //HTML table data
                        html.Append("<td>");
                        html.Append(row[column.ColumnName]);
                        html.Append("<br />");
                        html.Append("</td>");
                    }
                    //HTML table row header end
                    html.Append("</tr>");
                }

                //HTML Table End tag
                html.Append("</table>");

                //Append the HTML string to Placeholder.
                PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
            }
        }

    }
}