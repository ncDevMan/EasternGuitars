-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2015 at 06:14 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `alohainventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `conn_customers`
--

CREATE TABLE IF NOT EXISTS `conn_customers` (
  `customerID` int(64) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `phoneNumber` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`customerID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `conn_customers`
--

INSERT INTO `conn_customers` (`customerID`, `firstName`, `lastName`, `address`, `email`, `phoneNumber`) VALUES
(1, 'test', 'test', 'test', 'test', 'test'),
(4, '', '', '', '', ''),
(5, 'f', 'f', 'f', 'f', 'f'),
(8, 't', 't', 't', 't@t.com', '8597852998'),
(10, 'Mark', 'Twain', '3453', 'mt@gmail.com', '2616116516'),
(11, 'Eric', 'Hampton', '53534', 'et@gmail.com', '624196516');

-- --------------------------------------------------------

--
-- Table structure for table `conn_guitars`
--

CREATE TABLE IF NOT EXISTS `conn_guitars` (
  `guitarID` int(11) NOT NULL AUTO_INCREMENT,
  `supplierID` int(11) NOT NULL,
  `model` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `partID` int(11) NOT NULL DEFAULT '0',
  KEY `guitarID` (`guitarID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `conn_guitars`
--

INSERT INTO `conn_guitars` (`guitarID`, `supplierID`, `model`, `name`, `price`, `partID`) VALUES
(4, 1, 'D-15', 'Martin', '1,500.00', 0),
(12, 1, 'D-18', 'Martin', '1,800.00', 0),
(13, 2, '914CE', 'Taylor', '4,300.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `conn_orders`
--

CREATE TABLE IF NOT EXISTS `conn_orders` (
  `orderID` int(11) NOT NULL AUTO_INCREMENT,
  `customerID` int(11) NOT NULL,
  `shippingNumber` int(11) NOT NULL,
  `totalPrice` double NOT NULL,
  `totalParts` int(64) NOT NULL,
  `monthOfOrder` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`orderID`),
  UNIQUE KEY `customerID` (`customerID`,`shippingNumber`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `conn_orders`
--

INSERT INTO `conn_orders` (`orderID`, `customerID`, `shippingNumber`, `totalPrice`, `totalParts`, `monthOfOrder`) VALUES
(1, 0, 47100, 1518, 2, 'November'),
(2, 0, 15546, 4318, 2, 'November'),
(3, 0, 8501, 1500, 0, 'November'),
(4, 0, 97899, 1520, 2, 'November'),
(5, 0, 79996, 1822, 2, 'November'),
(6, 0, 6356, 1522, 2, 'November');

-- --------------------------------------------------------

--
-- Table structure for table `conn_parts`
--

CREATE TABLE IF NOT EXISTS `conn_parts` (
  `partID` int(11) NOT NULL AUTO_INCREMENT,
  `supplierID` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  KEY `partID` (`partID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `conn_parts`
--

INSERT INTO `conn_parts` (`partID`, `supplierID`, `name`, `price`) VALUES
(1, 1, 'Martin Acoustic Light Guitar Strings', '7.00'),
(8, 1, 'Martin Acoustic Medium Guitar Strings', '7.00'),
(9, 1, 'Martin Acoustic Heavy Guitar Strings', '9.00'),
(10, 1, 'Martin Black Bridge Pins', '13.00'),
(11, 1, 'Martin White Bridge Pins', '13.00');

-- --------------------------------------------------------

--
-- Table structure for table `conn_suppliers`
--

CREATE TABLE IF NOT EXISTS `conn_suppliers` (
  `supplerID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `phoneNumber` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`supplerID`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `conn_suppliers`
--

INSERT INTO `conn_suppliers` (`supplerID`, `name`, `address`, `phoneNumber`) VALUES
(1, 'Martin', '510 Sycamore Street, Nazareth, Pennsylvania 18064', '610-759-2837'),
(2, 'Taylor', 'El Cajon, California', '1 (800) 943-6782');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
